<?php

/**
* @file
* uw_lib_primo_search.context.inc
*/

/**
* Implements hook_context_default_contexts().
*/
function uw_lib_primo_search_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'primo_search';
  $context->description = 'Show Primo library catalogue search on home page';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_lib_primo_search-uw_lib_primo_search_form' => array(
          'module' => 'uw_lib_primo_search',
          'delta' => 'uw_lib_primo_search_form',
          'region' => 'content',
          'weight' => '-47',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Show Primo library catalogue search on home page');
  $export['primo_search'] = $context;

  return $export;
}

