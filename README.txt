INTRODUCTION
------------
The Library Primo Search Module is a block module that presents
a form for searching the UWaterloo Library online catalogue (Primo).


REQUIREMENTS 
------------

- There are no additional modules required.


INSTALLATION
------------

- Download the module from https://git.uwaterloo.ca/graham.faulkner/uw_lib_primo_search
  into your site's modules directory, and enable the module.


CONFIGURATION
-------------

- Add the block (it is titled "Find books and more") to the desired page(s) on your site
  via the Admin | Structure | Block page (i.e. administrator access is required).


MAINTAINERS
-----------

- The module is maintained by Graham Faulkner (graham.faulkner@uwaterloo.ca)


