<?php

/**
* @file
* uw_lib_primo_search.features.inc
*/

/**
* Implements hook_ctools_plugin_api().
*/
function uw_lib_primo_search_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
}

